package com.example.demo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Item;
import com.example.demo.model.User;

@RestController
@CrossOrigin

public class UserController {
	@GetMapping("/user")
	public User getUser() {
		User user = new User(1, "John");
		Item item = new Item(2, "book", user);
		user.addItem(item);
		return user;
	}


}
